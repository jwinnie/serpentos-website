---
title: "Performance and Efficiency at the Forefront"
date: 2021-08-02T12:20:34+10:00
draft: false
Callout: "Pushing the limits of what's possible"
---

Performance is one of the core values of Serpent OS, but it's important to reflect on what exactly that means. It is common for
people to think of performance in terms of benchmarks, where one can use all kinds of tricks to eke out that final 1%. Performance
in Serpent OS means increasing the performance of binaries and libraries we distribute to you in **all** circumstances. For example
a 1% increase in performance at a 10% power increase is an extremely poor default, but would be suitable for extreme use cases. By
targeting the performance of software, it will run faster whether you tune the kernel for `performance` or `powersave`.

Serpent OS achieves this by:

 - Moving away from `x86_64-generic` baseline to the newer `AVX2` CPUs by default
 - Preferring 128 vector width for `AVX` workloads by default, only increasing it where it makes a real difference (such as math or
   machine learning software)
   - This avoids switching penalties between `SSE` and `AVX` workloads
 - `clang` compiler by default for packages
   - The ability to use `gcc` for packages where a performance deficit is found
 - Integrating profile guided optimization (PGO) workloads into `boulder`
   - Including context sensitive 2 stage clang PGO
 - Using link time optimizations (LTO) where possible
 - Allowing custom per package compiler tuning
   - `boulder` makes this convenient by integrating many tuning options
 - Integrating [benchmarking-tools](https://github.com/sunnyflunk/benchmarking-tools) into the build system
   - This allows users to test the performance on a variety of hardware configurations

Check out this [blog post](../blog/2021/08/02/initial-performance-testing) to see some of these features in action.

#### Integrating benchmarking-tools

Solid benchmarking is a challenging process and requires a strong methodology and understanding what is creating the difference in
results. By integrating `benchmarking-tools` into Serpent OS, it allows us to start testing the performance already! Some key
features of `benchmarking-tools` are:

 - Actually tests the software installed on your system by the distribution
 - Very few requirements to run in a minimal environment
   - Can easily test other distros in a chroot to compare `clang` and `gcc` performance
   - Compare configuration and kernel differences between a chroot and host results
 - Integrated `perf` result generation to understand the performance differences
 - Accessible to all users so they may run the same tests, or even add their own
