---
title: "Making Smarter Decisions behind the Scenes"
date: 2021-06-22T11:43:54+10:00
draft: false
Callout: "Solving problems before they become an issue"
---

Subscriptions are a way to make your life easier without needing to know the ins and outs of the package manager or OS. This
enables packages to be automatically installed (or not installed) when smart conditions are met. You may have your own ideas
where subscriptions would make life easier for users. By using the collective efforts of our community, subscriptions will
allow you to:

 - Reduce unneeded dependencies (smaller updates)
 - ​Ensure you have the right kernel modules installed for your programs
 - Enable custom subscriptions for packages
   - Can have a minimal version of the same package with fewer features and dependencies
   - Have a `minimal` subscription for a lighter system (but some features will no longer be available)
 - ​Subscribing to language and translation files. Extra files don't need to be installed
 - Capability based subscriptions (i.e. hardware specific packages)

There are many possibilities in how you can use subscriptions. While a powerful feature that you can customize, it also just
works out of the box without you even knowing it exists!

Here's a few simple examples of how subscriptions can handle problem scenarios before you run into them:

 - Automatically install 32bit graphics drivers when you have `steam` or `wine` installed
 - Install the `dolphin` plugins for `nextcloud` when you have both programs installed, but don't install all the `KDE`
   dependencies of the plugins when you don't use `dolphin`
 - Automatically install the `VA`/`VA-VPI` packages for your hardware, but not all versions for all hardware
